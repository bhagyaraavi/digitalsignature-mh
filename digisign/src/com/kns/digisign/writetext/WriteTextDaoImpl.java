package com.kns.digisign.writetext;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.apache.log4j.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.kns.digisign.signpdf.SignPdfImpl;
/**
 * 
 * @author Manjunath
 *
 */
public class WriteTextDaoImpl implements WriteTextDao{
	private static final Logger log= Logger.getLogger(WriteTextDaoImpl.class);
	
	public String AddTextToPdf(PdfReader pdfReader,PdfStamper pdfStamper,String copyName,Object inte) throws KeyStoreException, NoSuchAlgorithmException, CertificateException{
		log.info(" Inside WriteTextDaoImpl -> AddTextToPdf() ");
		try {
			 //Create BaseFont instance.
		    BaseFont baseFont = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
		    //Get the number of pages in pdf.
		    int pagesO = pdfReader.getNumberOfPages(); 
		    //Iterate the pdf through pages.
		    for(int i=1; i<=pagesO; i++) { 
			//Contain the pdf data.
			PdfContentByte pageContentByte = pdfStamper.getOverContent(i);
			pageContentByte.beginText();
			//Set text font and size.
			pageContentByte.setColorFill(new BaseColor(255,0,0));
			pageContentByte.setFontAndSize(baseFont, 14);
			pageContentByte.setTextMatrix(505, 800);
			//Write text
			pageContentByte.showText(copyName);
			pageContentByte.endText();
			if(!copyName.equalsIgnoreCase("Original")){
				pageContentByte.beginText();
				//Set text font and size.
				pageContentByte.setColorFill(new BaseColor(0,0,0));
				pageContentByte.setFontAndSize(baseFont, 14);
				pageContentByte.setTextMatrix(400, 50);
				//Write text
				pageContentByte.showText("System generated invoice.");
				pageContentByte.setTextMatrix(400, 35);
				pageContentByte.showText("Hence, no signature required.");
				pageContentByte.endText();
			}
		    }
		    return "success";
		} catch (DocumentException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.info(" WriteTextDaoImpl -> AddTextToPdf() -> DocumentException | IOException -> "+e.getMessage());
			return "Error";
		}
	   
		
	}

}
