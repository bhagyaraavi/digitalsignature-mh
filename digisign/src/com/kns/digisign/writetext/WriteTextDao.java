package com.kns.digisign.writetext;

import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
/**
 * 
 * @author Manjunath
 *
 */
public interface WriteTextDao {
	
	public String AddTextToPdf(PdfReader pdfReader,PdfStamper pdfStamper,String copyName,Object inte)throws KeyStoreException,
	NoSuchAlgorithmException, CertificateException;
	

}
