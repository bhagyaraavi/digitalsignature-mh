package com.kns.digisign.fileupload;


import java.io.*;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.security.CrlClient;
import com.itextpdf.text.pdf.security.CrlClientOnline;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;
import com.kns.digisign.controller.EmailSender;
import com.kns.digisign.controller.MonitorDirectory;
import com.kns.digisign.controller.ReadPDFData;
import com.kns.digisign.dto.InvoiceDataDto;
import com.kns.digisign.service.ServiceClass;
import com.kns.digisign.service.ServiceClassDao;
import com.kns.digisign.signpdf.SignPdf;
import com.kns.digisign.signpdf.SignPdfImpl;
import com.kns.digisign.writetext.WriteTextDao;
import com.kns.digisign.writetext.WriteTextDaoImpl;

import sun.security.pkcs11.SunPKCS11;


/**
 * 
 * @author Manjunath Service method to add text, signature and merging the files
 *
 */
public class FileUploadService {
	private static final Logger log= Logger.getLogger(FileUploadService.class);
	public String SRC = null;
	public String DEST = null;
	// hardcoded the  USB digital device password because the client is going to use one device and system should need to run this process by 24/7 with out user login 
	public static String password =null; 
	WriteTextDao writeTextDao = new WriteTextDaoImpl();
	SignPdf signPdf = new SignPdfImpl();
	ServiceClass serviceClass = new ServiceClassDao();
	String processFileName = null;
	MonitorDirectory monitorDirectory = new MonitorDirectory();
	
	/**
	 * Created BY Bhagya on Sep 29th, 2020
	 * Service for to create the digital signature to invoices
	 */
	public void createSignatureToInvoice(ArrayList<File> invoicePDFFiles,String folderName,String invoiceFolderPath) {
		
		
		try {
			
				for (File invoicePDFFile : invoicePDFFiles) {
					
					String res = null;
					
					// looping list of files in the selected folder 
					
						//allowing only pdf files for signature process 
					if(invoicePDFFile.getName().toLowerCase().endsWith(".pdf")){
						
							// adding digital signature to final pdf and saving it in signedpdf folder
							SRC = invoicePDFFile.getAbsolutePath();
							
							Map<String,String> invoiceDataMap=renameTheSignedPDFFile(folderName);
							processFileName=invoiceDataMap.get("renamedFileName");
							log.info(" FileUploadService -> File NAme -> "+processFileName);
							String companyName=invoiceDataMap.get("companyName");
							
							if(companyName.contains("Ford") || companyName.contains("FORD")) {
								File fordDir = new File("C:\\Users\\1190401\\signedpdf\\Ford\\");
								if (!fordDir.exists()) {
									fordDir.mkdir();
								}
								DEST = fordDir+"\\"+ processFileName;
							
							}
							else {
								File mahindraDir = new File("C:\\Users\\1190401\\signedpdf\\Mahindra\\");
								if (!mahindraDir.exists()) {
									mahindraDir.mkdir();
								}
								DEST = mahindraDir+"\\"+ processFileName;
							}
							
							log.info(" FileUploadService -> Destination File location -> "+DEST);
							String pin = password;
							SunPKCS11 providerPKCS11 = new SunPKCS11("C:\\Users\\1190401\\processfile\\cfgfile\\pkcs11.cfg");
							Security.addProvider(providerPKCS11);
							KeyStore ks = KeyStore.getInstance("PKCS11");
							char[] pass = pin.toCharArray();
							ks.load(null, pass);
							Enumeration<String> aliases = ks.aliases();
							List<String> accessCode = new ArrayList<>();
							log.info(" FileUploadService -> Is Access Codes are available -> "+aliases.hasMoreElements());
							while (aliases.hasMoreElements()) {
								accessCode.add(aliases.nextElement());	
							}
							
							String dll36Code = null;
							for(String checkFormat: accessCode){
								
								log.info(" FileUploadService -> Access code loop -> access code -> "+checkFormat);
								if(checkFormat.contains("MADHAV")){ //MADHAV ANAND KULKARNI's Capricorn CA 2014 ID
									log.info("FileUploadService -> If check format satisfies or matches");
									dll36Code = checkFormat;
								}
					    		//boolean isCorrectFormat = checkFormat.matches("\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}");
					    		
					    		//if(isCorrectFormat==true){
					    			//log.info("FileUploadService -> If check format satisfies or matches");
					    		//	dll36Code = checkFormat;
					    		//}
									
					    		//else {
					    		//	log.info("FileUploadService -> If check format Not satisfies or miss matches");
					    		//	dll36Code = checkFormat; 
					    		//	}
									 
					    	}
							
							log.info("FileUploadService-> Before response "+providerPKCS11.getName() +" dll 36 code "+ dll36Code);
							res = smartcardsign(providerPKCS11.getName(), ks,dll36Code);
							log.info("FileUploadService -> Response -> "+res);
							// Once after signed invoice pdf has been created, sending an email to the customer with invoice attachment
							
													 
							if(res.equalsIgnoreCase("success")) {
								
								if(companyName.contains("Ford") || companyName.contains("FORD")) {
									EmailSender.sendEmail(DEST);
								}
								
								
								try {
									Thread.sleep(1000); // using thread here for to complete the email process
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								// delete the file form uploaded folder location
								
								File processedFile=new File(invoiceFolderPath+"\\"+invoicePDFFile.getName());
								MonitorDirectory.deleteFileFromFolder(processedFile);
								log.info(" FileUploadService -> File Deleted Successfully -> "+invoiceFolderPath+"\\"+invoicePDFFile.getName());
								
							}
					}
					
					} //for
				
				} //try
						catch (IOException e1) {
							log.info(" FileUploadService ->  IOException -> "+e1.getMessage());
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, processFileName + " File is corrupted.", " PDFFile Error",
									JOptionPane.ERROR_MESSAGE);
						}
						catch (DocumentException e1) {
							log.info(" FileUploadService ->  DocumentException -> "+e1.getMessage());
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, "Document Exception occurred", "DocumentException Exception",
									JOptionPane.ERROR_MESSAGE);
						} 
						catch (NoSuchAlgorithmException e1) {
							log.info(" FileUploadService ->  NoSuchAlgorithmException -> "+e1.getMessage());
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, "Algorithm Exception occurred", "AlgorithmException Exception",
									JOptionPane.ERROR_MESSAGE);
						} 
						catch (CertificateException e1) {
							log.info(" FileUploadService ->  CertificateException -> "+e1.getMessage());
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, "Certificate Exception occurred", "CertificateException Exception",
									JOptionPane.ERROR_MESSAGE);
						} 
						catch (KeyStoreException e1) {
							log.info(" FileUploadService ->  KeyStoreException -> "+e1.getMessage());
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, "Please insert Usb Drive to continue", "KeyStore Error",
									JOptionPane.ERROR_MESSAGE);
						} 
						catch (GeneralSecurityException e1) {
							log.info(" FileUploadService ->  GeneralSecurityException -> "+e1.getMessage());
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null, "GeneralSecurity Exception occurred", "GeneralSecurity Error",
									JOptionPane.ERROR_MESSAGE);
						} 
						
	} // method

	/**
	 * created By Manjunath to get details of provider , keystore, alias
	 * 
	 * @param provider
	 * @param ks
	 * @param alias
	 * @throws GeneralSecurityException
	 * @throws IOException
	 * @throws DocumentException
	 */
	@SuppressWarnings("deprecation")
	public String smartcardsign(String provider, KeyStore ks, String alias)
			throws GeneralSecurityException, IOException, DocumentException {
		log.info("FileUploadService -> Inside smartcardsign service");
		PrivateKey pk = (PrivateKey) ks.getKey(alias, null);
		Certificate[] chain = ks.getCertificateChain(alias);
		OcspClient ocspClient = new OcspClientBouncyCastle();
		//List<CrlClient> crlList = new ArrayList<CrlClient>(); // commented the crlList and passing the crlList as null while adding sign
		//crlList.add(new CrlClientOnline(chain)); // Because CrlList taking more memory for signed file. As per client request, reduced the file size
		String res = signPdf.sign(SRC, String.format(DEST, alias), chain, pk, DigestAlgorithms.SHA256, provider, CryptoStandard.CMS,
				null, null, null, ocspClient, null, 0);
		return res;
	}
	/**
	 * Created By Bhagya on Sep 24th, 2020
	 * @return
	 * 
	 * Service for to rename the file 
	 * The rename syntax is based on mann-hummel client requirement
	 */
	public Map<String,String> renameTheSignedPDFFile(String folderName) {
		log.info("FileUploadService -> renameTheSignedPDFFile()");
		String outputFileName="";
		InvoiceDataDto invoiceDataDto=null;
		Map<String,String> invoiceDataMap=new HashMap<String, String>();
		try {
			invoiceDataDto = ReadPDFData.getPageContent(SRC);
			String invoiceType=invoiceDataDto.getInvoiceType().replace(" ", "");
			String locationCode="";
			if(invoiceDataDto.getVendorCode().equalsIgnoreCase("INFORD95AO")) {
				locationCode="2";
			}
			else {
				locationCode="1";
			}
			String companyName=invoiceDataDto.getCompanyName();
			System.out.println(" rename service of company name "+companyName);
			if(companyName.contains("Ford") || companyName.contains("FORD")) {
				System.out.println(" Inside ford company if condition");
				// If it is price variance folder
				if(folderName.contains("PV")) {
					if(invoiceType.contains("DEBITNOTE")) {
						outputFileName="P6"+"_"+"4"+"_"+locationCode+'_'+invoiceDataDto.getSupplierCode()+'_'+invoiceDataDto.getInvoiceNumber()+".pdf";
					}
					else if(invoiceType.contains("CREDITNOTE")) {
						outputFileName="P6"+"_"+"3"+"_"+locationCode+'_'+invoiceDataDto.getSupplierCode()+'_'+invoiceDataDto.getInvoiceNumber()+".pdf";
					}
				}
				else {
					// here it is retro invoices folder
					if(invoiceType.contains("TAXINVOICE")) {
						outputFileName="E6"+"_"+locationCode+'_'+invoiceDataDto.getSupplierCode()+'_'+invoiceDataDto.getInvoiceNumber()+".pdf";
					}
					else if(invoiceType.contains("DEBITNOTE")) {
						outputFileName="R8"+"_"+"4"+"_"+locationCode+'_'+invoiceDataDto.getSupplierCode()+'_'+invoiceDataDto.getInvoiceNumber()+".pdf";
					}
					else if(invoiceType.contains("CREDITNOTE")) {
						outputFileName="R8"+"_"+"3"+"_"+locationCode+'_'+invoiceDataDto.getSupplierCode()+'_'+invoiceDataDto.getInvoiceNumber()+".pdf";
					}
				}
			}
			else {
				System.out.println("Inside mahindra company");
				// Mahindra Company
				String invoiceDate=invoiceDataDto.getDateOfIssue().replace(".", "");
				 System.out.println(" invoice date "+invoiceDate);
					outputFileName=invoiceDataDto.getSupplierCode()+'_'+invoiceDataDto.getInvoiceNumber()+'_'+invoiceDate+".pdf";
				
			}
			
			
			invoiceDataMap.put("renamedFileName", outputFileName);
			invoiceDataMap.put("companyName",invoiceDataDto.getCompanyName());
			invoiceDataMap.put("toEmail", invoiceDataDto.getToEmail());
			System.out.println("output file name "+outputFileName);
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.info("FileUploadService -> renameTheSignedPDFFile() -> Exception -> "+e.getMessage());
		}
		
		return invoiceDataMap;
	}
	
	public void getPasssword(String[] args) {
		password=args[0];
		
	}
}
