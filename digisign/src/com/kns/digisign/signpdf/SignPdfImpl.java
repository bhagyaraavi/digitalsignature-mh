package com.kns.digisign.signpdf;

import java.io.File;
import java.io.FileOutputStream;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Collection;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;

import org.apache.log4j.Logger;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfSignatureAppearance.RenderingMode;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.CrlClient;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
/**
 * 
 * @author Manjunath
 *
 */
public class SignPdfImpl implements SignPdf {
	
	private static final Logger log= Logger.getLogger(SignPdfImpl.class);
	
	@SuppressWarnings("deprecation")
	public String sign(String src, String dest, Certificate[] chain, PrivateKey pk, String digestAlgorithm,
			String provider, CryptoStandard subfilter, String reason, String location, Collection<CrlClient> crlList,
			OcspClient ocspClient, TSAClient tsaClient, int estimatedSize)
					throws GeneralSecurityException, IOException, DocumentException {
		log.info(" SignPdfImpl -> Inside sign()");
		// Creating the reader and the stamper
		PdfReader reader = new PdfReader(src);
		FileOutputStream os = new FileOutputStream(dest);
		PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0');
		
		log.info(" SignPdfImpl -> Sign() ->Created the reader and the stamper");
		// Creating the appearance
		PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
		appearance.setReason("I approved this document");
		appearance.setLocation("Bangalore");
		appearance.setAcro6Layers(false);
		appearance.setLayer4Text(PdfSignatureAppearance.questionMark);
		appearance.setRenderingMode(RenderingMode.NAME_AND_DESCRIPTION);
		//appearance.setVisibleSignature(new Rectangle(820,80,560, 130), 1, "SignHere"); //- mann-hummel -old
		//appearance.setVisibleSignature(new Rectangle(820,40,560, 75), 1, "SignHere");
		//appearance.setVisibleSignature(new Rectangle(818,35,562, 80), 1, "E-Lock-Signature1");
		appearance.setVisibleSignature(new Rectangle(820,45,580, 100), 1, "E-Lock-Signature1");
		log.info(" SignPdfImpl -> Sign() ->Created the appearance");
		// Creating the signature
		ExternalSignature pks = new PrivateKeySignature(pk, digestAlgorithm, provider);
		ExternalDigest digest = new BouncyCastleDigest();
		MakeSignature.signDetached(appearance, digest, pks, chain, crlList, ocspClient, tsaClient, estimatedSize,subfilter);
		
		stamper.close();
		os.flush();
		os.close();
		reader.close();
		log.info(" SignPdfImpl -> Sign() ->Created the signature");
		return "success";

	}


}
