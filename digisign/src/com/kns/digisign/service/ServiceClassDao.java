package com.kns.digisign.service;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;

import org.apache.log4j.Logger;

import com.kns.digisign.fileupload.FileUploadService;

import sun.security.pkcs11.SunPKCS11;
/**
 * 
 * @author Manjunath 
 *
 */
public class ServiceClassDao implements ServiceClass {
	private static final Logger log= Logger.getLogger(ServiceClassDao.class);
	char[] pass = null;
	KeyStore ks = null;
	@Override
	public String getLoginUpdate(String password) {
		log.info(" ServiceClassDao -> getLoginUpdate() ");
		pass = password.toCharArray();
		SunPKCS11 providerPKCS11 = new SunPKCS11("C:\\Users\\1190401\\processfile\\cfgfile\\pkcs11.cfg");
		Security.addProvider(providerPKCS11);
		try {
			ks = KeyStore.getInstance("PKCS11", providerPKCS11);
			ks.load(null, pass);
			return "Success";
		} catch (KeyStoreException e) {
			log.info(" KeyStore Exception ->"+e.getMessage());
			e.printStackTrace();
			return "Nousbdrive";
		} catch (NoSuchAlgorithmException e) {
			log.info(" NoSuchAlgorithm Exception ->"+e.getMessage());
			e.printStackTrace();
			return "Algorithm";
		} catch (CertificateException e) {
			log.info(" Certificate Exception ->"+e.getMessage());
			e.printStackTrace();
			return "Certificate";
		} catch (IOException e) {
			log.info(" IO Exception ->"+e.getMessage());
			e.printStackTrace();
			return "Error";
		}
		catch (Exception e) {
			log.info(" ServiceClassDao -> getLoginUpdate()-> Inside Exception ->"+e.getMessage());
			e.printStackTrace();
			return "ErrorApp";
		}
	}

}
