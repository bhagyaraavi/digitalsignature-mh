package com.kns.digisign.utility.scheduler;

import org.apache.log4j.Logger;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

/***
 * 
 * @author Bhagya
 * 
 * Scheduler class for to monitor directory
 */
public class MonitorDirectoryScheduler{
	private static final Logger log= Logger.getLogger(MonitorDirectoryScheduler.class);
	
	/**
	 * Created By Bhagya
	 * Scheduler class for to save user work hours
	 */
	public void schedulerService(){
		log.info(" MonitorDirectoryScheduler -> schedulerService()");
		try {
		 JobDetail job = JobBuilder.newJob(MonitorDirectoryJob.class)
		.withIdentity("dummyJobName", "group1").build();

    	Trigger trigger = TriggerBuilder
		.newTrigger()
		.withIdentity("dummyTriggerName", "group1")
		.withSchedule(
			CronScheduleBuilder.cronSchedule("0 0/30 * * * ?")) // trigger has been set to run at every 30 minutes
		.build();
   	//schedule it
    	Scheduler scheduler = new StdSchedulerFactory().getScheduler();
			scheduler.start();
	    	scheduler.scheduleJob(job, trigger);
		} 
		catch (SchedulerException e) {
			log.info(" MonitorDirectoryScheduler -> SchedulerException -> "+e.toString());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	catch(Exception e) {
    		log.info(" MonitorDirectoryScheduler -> Exception -> "+e.toString());
    		e.printStackTrace();
    	}

    }
}