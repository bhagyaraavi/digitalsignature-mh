package com.kns.digisign.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.itextpdf.text.Rectangle;
import com.kns.digisign.fileupload.FileUploadService;
import com.kns.digisign.utility.scheduler.MonitorDirectoryScheduler;


/**
 * 
 * @author Bhagya on Sep 28th, 2020
 * Class for to monitor the directory for newly uploaded invoices
 */
public class MonitorDirectory {
	
	private static final Logger log= Logger.getLogger(MonitorDirectory.class);
	static MonitorDirectoryScheduler monitorDirectoryScheduler=new MonitorDirectoryScheduler();
	/**
	 *  Created By BHagya on Sep 29th, 2020
	 *  Service for to track or watch the directory files created events
	 *  1. getting the files from directory
	 *  2. copying the files to another directory as processed files
	 *      getting the filepaths from processed directory and using the same files for to apply digi sign
	 *  3. deleting files from old or first directory
	 */
	
	public static void watchDirectoryEvents(){
		log.info(" MonitorDirectory -> watchDirectoryEvents() ");
		try {
			Map<String,String> invoicesFoldersMap=getInvoicesFolders();
			for(Map.Entry<String,String> entry :invoicesFoldersMap.entrySet()) {
				FileUploadService fileUploadService=new FileUploadService();
				//String folderPath="E:\\KNS Work\\Projects\\Digital Signature\\Mann-Hummel\\Sample Invoices\\";
				//String folderPath="\\fs-in-ba-2\\SC-IN\\Mahindra\\";
				String folderPath=entry.getValue();
				//String folderPath="\\\\fs-in-ba-2\\SC-IN\\Ford Invoice\\";
				File selectedFolder = new File(folderPath);
				File[] uploadedFiles = selectedFolder.listFiles();
				log.info("FileUploadService -> PDF Files Size of selected folder -> "+uploadedFiles.length);
				ArrayList<File> signatureFiles=copyFileToFolder(uploadedFiles,entry.getValue(),entry.getKey());
				fileUploadService.createSignatureToInvoice(signatureFiles,entry.getKey(),entry.getValue());
				//deleteFilesFromFolder(uploadedFiles);
			}
				
		}
		
		catch (Exception e) {
			log.info(" MonitorDirectory -> Exception() ->  "+e.getMessage());
			e.printStackTrace();
		}
		
	} // method
	/**
	 * Created By BHagya on October 05th, 2020
	 * @param uploadedFiles
	 * @return
	 * 
	 * Service for to copy the files to folder ( copying the original files to processed folder)
	 * and getting the file paths from the processed folder and using the same files of processed folder for to apply the digital signature
	 */
	
	public static ArrayList<File> copyFileToFolder(File[] uploadedFiles,String invoiceFolderPath,String folderName ) {
		log.info(" MonitorDirectory -> copyFileToFolder() ");
		ArrayList<File> filepaths=new ArrayList<File>();
		for( File file:uploadedFiles) {
		//String sourcePath="E:\\KNS Work\\Projects\\Digital Signature\\Mann-Hummel\\Sample Invoices\\"+file.getName();
		//String destinationPath="E:\\KNS Work\\Projects\\Digital Signature\\Mann-Hummel\\Processed_Files\\"+file.getName();
		//String sourcePath="\\fs-in-ba-2\\SC-IN\\Mahindra\\"+file.getName();
		//String destinationPath="\\fs-in-ba-2\\SC-IN\\Complete invoices\\Mahindra\\"+file.getName();
		//String sourcePath="\\\\fs-in-ba-2\\SC-IN\\Ford Invoice\\";
		//String destinationPath="\\fs-in-ba-2\\SC-IN\\Processed_Files\\"+file.getName();
		String sourcePath=invoiceFolderPath+file.getName();
		String destinationPath="C:\\Users\\1190401\\Completed Invoices\\"+folderName+"\\"+file.getName();
		Path source = Paths.get(sourcePath);
        Path target = Paths.get(destinationPath);
       
		try {
			Files.copy(source, target,StandardCopyOption.REPLACE_EXISTING);
		        filepaths.add(new File(destinationPath));
		} catch (IOException e) {
			log.info(" MonitorDirectory -> copyFileToFolder() -> IOException() -> "+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) {
			log.info(" MonitorDirectory -> copyFileToFolder() -> Exception() -> "+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		return filepaths;
	}
	/**
	 * Created By Bhagya on October 05th, 2020
	 * @param uploadedFiles
	 * 
	 * Service for to delete the files from folder
	 */
	
	public static void deleteFilesFromFolder(File[] uploadedFiles) {
		log.info(" MonitorDirectory -> deleteFileToFolder() ");
		for( File file:uploadedFiles) {
			try {
				System.gc();
				file.delete();
			} catch (Exception e) {
				log.info(" MonitorDirectory -> deleteFileToFolder() -> Exception -> "+e.getMessage());
				
				e.printStackTrace();
			}
		}
	}
	/**
	 * Created By Bhagya
	 * Service for to delete the processed file from the uploaded folder
	 */
	public static void deleteFileFromFolder(File processedFile) {
		log.info(" MonitorDirectory -> deleteFileFromFolder() ");
		
			try {
				System.gc();
				processedFile.delete();
			} catch (Exception e) {
				log.info(" MonitorDirectory -> deleteFileFromFolder() -> Exception -> "+e.getMessage());
				e.printStackTrace();
			}
		
	}
	/**
	 * Created By Bhagya on october 07th,2020
	 * @return
	 * 
	 * The mann hummel invoices - maintaining the following folders
	 * Ford Invoices - will contains tax invoices, credit and debit notes of FOrd Company
	 * Ford Invoices PV - will contains the credit and debit notes OF Ford Company
	 * Mahindra - will contains the tax invoices of MAHINDRA Company
	 */
	public static Map<String,String> getInvoicesFolders(){
		log.info("MonitorDirectory ->getInvoicesFolders()");
		Map<String,String> invoicesFoldersMap=new HashMap<String, String>();
		invoicesFoldersMap.put("Ford Invoices", "C:\\Users\\1190401\\Ford Invoices\\");
		invoicesFoldersMap.put("Ford Invoices PV", "C:\\Users\\1190401\\Ford Invoices PV\\");
		invoicesFoldersMap.put("Mahindra", "C:\\Users\\1190401\\Mahindra\\");
		
		return invoicesFoldersMap;
	}
		
	/**
	 * Created By Bhagya
	 * Main method for to start the digital signature process
	 * @param args
	 * In this main method, at first we are calling directory events method ( to handle at first minute after run the app)
	 * then schedule the event for to run or watch the directory by every 10 minutes ( from second time, the schdeuler will run)
	 */
	public static void main(String args[]) {
			//watchDirectoryEvents();
			// using the scheduler service for to add the signature to the invoices by every 10 minutes
			//monitorDirectoryScheduler.schedulerService();
	}
	
	
	
} // class