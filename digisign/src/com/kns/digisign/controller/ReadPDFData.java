package com.kns.digisign.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.FilteredTextRenderListener;
import com.itextpdf.text.pdf.parser.LocationTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.itextpdf.text.pdf.parser.RegionTextRenderFilter;
import com.itextpdf.text.pdf.parser.RenderFilter;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.kns.digisign.dto.InvoiceDataDto;
import com.itextpdf.text.Rectangle;
/**
 * Created on September 24th, 2020
 * @author Bhagya
 * Class for to read required date from the invoice pdf based on the rectangle positions
 */
public class ReadPDFData{
	private static final Logger log= Logger.getLogger(ReadPDFData.class);
	
	public static InvoiceDataDto getPageContent(String sourceInvoiceFile) throws Exception
	{
		log.info(" ReadPDFData -> getPageContent");
		
		try {
		Integer pageNumber=1;
		InvoiceDataDto invoiceDataDto=new InvoiceDataDto();
	    //PdfReader reader = new PdfReader("E:\\KNS Work\\Projects\\Digital Signature\\Mann-Hummel\\Sample Invoices\\Mahindra sample invoice.pdf");
		PdfReader reader = new PdfReader(sourceInvoiceFile);
	    String output=new String();
	    Map<String,String> invoiceDataMap=new HashMap<String,String>();
	    Map<String,Rectangle> rectanglePositionsMap=new HashMap<String,Rectangle>();
	    // old invoices positions
		/*
		 * rectanglePositionsMap.put("invoiceType", new Rectangle(20,20, 40, 616));
		 * rectanglePositionsMap.put("invoiceNumber", new Rectangle(60,430, 50, 616));
		 * rectanglePositionsMap.put("dateOfIssue", new Rectangle(60,430, 80, 616));
		 * rectanglePositionsMap.put("vendorCode", new Rectangle(170,450, 160, 616));
		 * rectanglePositionsMap.put("companyName", new Rectangle(180,280, 170, 616));
		 * rectanglePositionsMap.put("supplierCode", new Rectangle(200,280, 210, 416));
		 */
	    // from email position is changing based on invoice type-- so hardcoding the from email
	    //rectanglePositionsMap.put("fromEmail", new Rectangle(130,430, 140, 616)); // tax invoice
	    //rectanglePositionsMap.put("fromEmail", new Rectangle(130,430, 120, 616)); // credit note and debit note
		/* rectanglePositionsMap.put("toEmail", new Rectangle(220,280, 230, 616)); */
	    
	    // Second old mann-hummel invoices positions
	    //rectanglePositionsMap.put("invoiceType", new Rectangle(20,20, 40, 616));
	    //rectanglePositionsMap.put("invoiceNumber", new Rectangle(60,430, 50, 616));
	    //rectanglePositionsMap.put("dateOfIssue", new Rectangle(60,430, 80, 616));
	    //rectanglePositionsMap.put("vendorCode", new Rectangle(190,450, 170, 616));
	    //rectanglePositionsMap.put("companyName", new Rectangle(200,220, 190, 616));
	    //rectanglePositionsMap.put("supplierCode", new Rectangle(240,280, 230, 416));
	    //rectanglePositionsMap.put("toEmail", new Rectangle(250,260, 240, 416));
	    
	    // new mann-hummel invoices positions
	   // rectanglePositionsMap.put("invoiceType", new Rectangle(20,20, 40, 616));
	    //rectanglePositionsMap.put("invoiceNumber", new Rectangle(60,430, 50, 616));
	    //rectanglePositionsMap.put("dateOfIssue", new Rectangle(60,430, 80, 616));
	    //rectanglePositionsMap.put("vendorCode", new Rectangle(160,450, 170, 616));
	    //rectanglePositionsMap.put("companyName", new Rectangle(180,220, 170, 616));
	    //rectanglePositionsMap.put("supplierCode", new Rectangle(210,280, 200, 416));
	    //rectanglePositionsMap.put("toEmail", new Rectangle(230,280, 220, 416));
	    
	    // Jan 2021 invoice layout changes
	    ///rectanglePositionsMap.put("invoiceType", new Rectangle(40,30, 50, 616));
	    ///rectanglePositionsMap.put("invoiceNumber", new Rectangle(60,440, 80, 616));
	    ///rectanglePositionsMap.put("dateOfIssue", new Rectangle(90,440, 80, 616));
	    //rectanglePositionsMap.put("vendorCode", new Rectangle(200,440, 190, 616));
	    //rectanglePositionsMap.put("companyName", new Rectangle(220,260, 200, 616));
	    //rectanglePositionsMap.put("supplierCode", new Rectangle(240,300, 250, 416));
	    //rectanglePositionsMap.put("toEmail", new Rectangle(260,280, 250, 416));
	    
	 // Jan 2021 on 25th modified the  invoice layout changes - client reading the invoices from SAP
	   // rectanglePositionsMap.put("invoiceType", new Rectangle(200,540, 400, 560));
	    //rectanglePositionsMap.put("invoiceNumber", new Rectangle(480,520,450, 530));
	    //rectanglePositionsMap.put("dateOfIssue", new Rectangle(488,500, 530, 520));
	    //rectanglePositionsMap.put("vendorCode", new Rectangle(80,400, 190, 390));
	    //rectanglePositionsMap.put("companyName", new Rectangle(80,380, 190, 390));
	    //rectanglePositionsMap.put("supplierCode", new Rectangle(330,360, 300, 340));
	    //rectanglePositionsMap.put("toEmail", new Rectangle(220,340, 190, 340));
	    
	 // Jan 2021, On April 06th invoice layout changes
	    rectanglePositionsMap.put("invoiceType", new Rectangle(40,30, 50, 616));
	    rectanglePositionsMap.put("invoiceNumber", new Rectangle(60,440, 80, 616));
	    rectanglePositionsMap.put("dateOfIssue", new Rectangle(90,440, 80, 616));
	    rectanglePositionsMap.put("vendorCode", new Rectangle(210,480, 200, 616));
	    rectanglePositionsMap.put("companyName", new Rectangle(230,450, 210, 616));
	    rectanglePositionsMap.put("supplierCode", new Rectangle(260,290, 250, 416));
	    //rectanglePositionsMap.put("toEmail", new Rectangle(260,280, 250, 416));
	        	Integer rectanglePositionsMapSize=rectanglePositionsMap.size();
	        	
	        	if(rectanglePositionsMapSize>0) {
	        	
	        		for (Map.Entry<String,Rectangle> entry :rectanglePositionsMap.entrySet()) //using map.entrySet() for iteration  
	        		{  
	        			Rectangle rect=entry.getValue();
	        			RenderFilter regionFilter = new RegionTextRenderFilter(rect);
	    	            FontRenderFilter fontFilter = new FontRenderFilter();
	    	            TextExtractionStrategy strategy = new FilteredTextRenderListener(
	    	                    new LocationTextExtractionStrategy(), regionFilter, fontFilter);
	    	            output=PdfTextExtractor.getTextFromPage(reader, pageNumber, strategy);
	    	            invoiceDataMap.put(entry.getKey(), output.toString());
	    	            System.out.println("  "+ entry.getKey() + " "+output.toString());
	        		}
	        		
	        	}
	        	
		        invoiceDataDto.setInvoiceType(invoiceDataMap.get("invoiceType"));
		        invoiceDataDto.setInvoiceNumber(invoiceDataMap.get("invoiceNumber").replace(":", "").replace(" ", ""));
		        invoiceDataDto.setDateOfIssue(invoiceDataMap.get("dateOfIssue").replace(":", ""));
		        invoiceDataDto.setCompanyName(invoiceDataMap.get("companyName"));
		        invoiceDataDto.setSupplierCode(invoiceDataMap.get("supplierCode"));
		        //invoiceDataDto.setFromEmail(invoiceDataMap.get("fromEmail"));
		      //  invoiceDataDto.setFromEmail("RASHMI.MK@MANN-HUMMEL.COM"); // hardcoded the email
		       // invoiceDataDto.setToEmail(invoiceDataMap.get("toEmail"));
		        invoiceDataDto.setVendorCode(invoiceDataMap.get("vendorCode"));
		        return invoiceDataDto;

	        } 
	        catch (OutOfMemoryError e) {
	        	log.info("ReadPDFData -> getPageContent -> OutOfMemoryError  "+e.getMessage());
	            e.printStackTrace();
	            throw new OutOfMemoryError();
	        }
	        catch(Exception e) {
	        	e.printStackTrace();
	        	log.info("ReadPDFData -> getPageContent -> Exception  "+e.getMessage());
	        	 throw new Exception();
	        }
		
	        
	   
	}
	
	public static void main (String args[]) throws Exception {
		try {
		InvoiceDataDto	invoiceDataDto=getPageContent("");
		
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}