package com.kns.digisign.controller;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;

import org.apache.log4j.Logger;

import com.kns.digisign.fileupload.FileUploadService;
import com.kns.digisign.service.ServiceClass;
import com.kns.digisign.service.ServiceClassDao;
import com.kns.digisign.utility.scheduler.MonitorDirectoryScheduler;

import javax.swing.JPasswordField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
/**
 * 
 * @author Manjunath 
 * Login window Main Class
 *
 */
public class MainClass {
	private static final Logger log= Logger.getLogger(MainClass.class);
	private JFrame frmLogin;
	private final JLabel lblDigitalSignaturePassword = new JLabel("Digital Signature");
	private JPasswordField passwordField;
	ServiceClass serviceClass = new ServiceClassDao();
	static MonitorDirectoryScheduler monitorDirectoryScheduler=new MonitorDirectoryScheduler();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		log.info(" Inside MainClass of main method");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainClass window = new MainClass();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Error while adding digital signature to pdf", "Pdf Signature Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainClass() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setBackground(Color.BLACK);
		frmLogin.setIconImage(Toolkit.getDefaultToolkit().getImage(MainClass.class.getResource("/images/proicon.jpg")));
		frmLogin.setTitle("Login Details");
		frmLogin.setBounds(200, 100, 500, 300);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		lblDigitalSignaturePassword.setForeground(Color.BLACK);
		lblDigitalSignaturePassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblDigitalSignaturePassword.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblDigitalSignaturePassword.setBounds(155, 11, 156, 31);
		frmLogin.getContentPane().add(lblDigitalSignaturePassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(155, 107, 156, 31);
		frmLogin.getContentPane().add(passwordField);
		
		JLabel lblEnterPassword = new JLabel("  Enter Password:");
		lblEnterPassword.setForeground(Color.BLACK);
		lblEnterPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblEnterPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnterPassword.setBounds(155, 53, 156, 43);
		frmLogin.getContentPane().add(lblEnterPassword);
		JButton btnLogin = new JButton("Login");
		btnLogin.setForeground(Color.BLACK);
		btnLogin.setBackground(Color.LIGHT_GRAY);
		btnLogin.addActionListener(new ActionListener() {
			String password = null;
			@SuppressWarnings({ "static-access", "deprecation" })
			public void actionPerformed(ActionEvent e) {
				log.info("MainClass -> Inside action listener of login");
				password = passwordField.getText();
				String[] args = {password}; 
				String result = serviceClass.getLoginUpdate(password);
				log.info(" MainClass ->  Login Result -> "+result);
				if(result.equalsIgnoreCase("success")){
					frmLogin.setVisible(false);
					JOptionPane.showMessageDialog(null,"Digital signature process has been started","Digital Sign",JOptionPane.ERROR_MESSAGE);
					FileUploadService fileUploadService =new FileUploadService();
					fileUploadService.getPasssword(args);
					MonitorDirectory.watchDirectoryEvents();
					monitorDirectoryScheduler.schedulerService();
					
					
				}else if(result.equalsIgnoreCase("nousbdrive")){
					passwordField.setText(null);
					JOptionPane.showMessageDialog(null,"Please insert Usb Drive to continue","Login Error",JOptionPane.ERROR_MESSAGE);
				}else if(result.equalsIgnoreCase("Algorithm")){
					passwordField.setText(null);
					JOptionPane.showMessageDialog(null,"Algorithm Exception occurred","Login Exception",JOptionPane.ERROR_MESSAGE);
				}else if(result.equalsIgnoreCase("Certificate")){
					passwordField.setText(null);
					JOptionPane.showMessageDialog(null,"Certificate Exception occurred","Login Exception",JOptionPane.ERROR_MESSAGE);
				}
				else if(result.equalsIgnoreCase("ErrorApp")){
					passwordField.setText(null);
					JOptionPane.showMessageDialog(null,"Something Went Wrong","Login Exception",JOptionPane.ERROR_MESSAGE);
				}
				else{
					passwordField.setText(null);
					JOptionPane.showMessageDialog(null,"Invalid Login Credential","Login Error",JOptionPane.ERROR_MESSAGE);
				}				
			}
		});
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnLogin.setBounds(89, 176, 89, 31);
		frmLogin.getContentPane().add(btnLogin);
		
		JButton btnClose = new JButton("Exit");
		btnClose.setForeground(Color.BLACK);
		btnClose.setBackground(Color.LIGHT_GRAY);
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFrame exitWindow = new JFrame("Exit");
				if(JOptionPane.showConfirmDialog(exitWindow, "Do you really want to exit!","Exit",
						JOptionPane.YES_NO_OPTION)== JOptionPane.YES_NO_OPTION){
					System.exit(0);
				}
			}
		});
		btnClose.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnClose.setBounds(287, 176, 89, 31);
		frmLogin.getContentPane().add(btnClose);
		
		JButton btnReset = new JButton("Reset");
		btnReset.setForeground(Color.BLACK);
		btnReset.setBackground(Color.LIGHT_GRAY);
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				passwordField.setText(null);
			}
		});
		btnReset.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnReset.setBounds(188, 177, 89, 29);
		frmLogin.getContentPane().add(btnReset);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(MainClass.class.getResource("/images/imgbkgd.jpg")));
		label.setBounds(0, 0, 484, 261);
		frmLogin.getContentPane().add(label);
	}
}
