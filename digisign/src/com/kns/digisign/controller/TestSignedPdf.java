package com.kns.digisign.controller;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.util.List;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.security.CertificateInfo;
import com.itextpdf.text.pdf.security.PdfPKCS7;

public class TestSignedPdf{
	
public static void testVerifySignedOutput() throws IOException, GeneralSecurityException
{
    System.out.println("\n\nsignedoutput.pdf\n================");
	
    try{
        PdfReader reader = new PdfReader("C:\\Users\\1190401\\signedpdf\\Ford\\E6_2_FXUWB_89133730.pdf");
        AcroFields acroFields = reader.getAcroFields();
        System.out.println("Sizd "+acroFields.getBlankSignatureNames().size());
        List<String> blankSignatureFields=acroFields.getBlankSignatureNames();
        for(String name:blankSignatureFields) {
        	System.out.println(" blank "+name);
        }
        List<String> names = acroFields.getSignatureNames();
        for (String name : names) {
           System.out.println("Signature name: " + name);
           System.out.println("Signature covers whole document: " + acroFields.signatureCoversWholeDocument(name));
           System.out.println("Document revision: " + acroFields.getRevision(name) + " of " + acroFields.getTotalRevisions());
           PdfPKCS7 pk = acroFields.verifySignature(name);
           System.out.println("Subject: " + CertificateInfo.getSubjectFields(pk.getSigningCertificate()));
           System.out.println("Document verifies: " + pk.verify());
        }
    }
    catch(Exception e) {
    	e.printStackTrace();
    }
}

	public static void main(String args[]) {
		try {
			testVerifySignedOutput();
		} catch (IOException | GeneralSecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}