package com.kns.digisign.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
 
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
 

/**
  * 
  * @author Bhagya
  * Implemented the email functionality for to send invoices to Ford Company 
  * Modified : Commented the password authentication , because mann-hummel have a separate authentication host to send an email
  * But when we testing in our local system, we are using test gmail account, for that we required password implementation code
  */
public class EmailSender {
	private static final Logger log= Logger.getLogger(EmailSender.class);
    public static void sendEmailWithAttachments(String host, String port,
            final String userName, String toAddress,String ccAddress,
            String subject, String message, String[] attachFiles,String password)
            throws AddressException, MessagingException {
        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
       // properties.put("mail.smtp.auth", "true"); // commented
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.ssl.protocols", "TLSv1.2");
        properties.put("mail.user", userName);
       // properties.put("mail.password", password); //commented
 
        // creates a new session with an authenticator
       // Authenticator auth = new Authenticator() {
       //    public PasswordAuthentication getPasswordAuthentication() {
       //         return new PasswordAuthentication(userName, password);
       //   }
      //  };
       
      // Session session = Session.getDefaultInstance(properties,auth);
       Session session = Session.getDefaultInstance(properties);
        // creates a new e-mail message
        Message msg = new MimeMessage(session);
 
        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
        InternetAddress[] ccAddresses = { new InternetAddress(ccAddress) };
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setRecipients(Message.RecipientType.CC, ccAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
       
        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html");
 
        // creates multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
 
        // adds attachments
        if (attachFiles != null && attachFiles.length > 0) {
            for (String filePath : attachFiles) {
                MimeBodyPart attachPart = new MimeBodyPart();
 
                try {
                    attachPart.attachFile(filePath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
 
                multipart.addBodyPart(attachPart);
            }
        }
 
        // sets the multi-part as e-mail's content
        msg.setContent(multipart);
 
        // sends the e-mail
       Transport.send(msg);
       
 
    }
    /**
     * Created By Bhagya
     * This service will get the email properties from digisign.properties file
     * and it will call the another service for to send an email
     */
    public static void sendEmail(String emailAttachmentPath){
		log.info(" EmailSender -> sendEmail() ");
    	
    	try {
    		InputStream in = ClassLoader.getSystemResourceAsStream("digisign.properties");
    		Properties props = new Properties();
    		props.load(in);
    		in.close();
    		String host=props.getProperty("host");
    		String port=props.getProperty("port");
    		String mailFrom=props.getProperty("mailFrom");
    		String password=props.getProperty("password");
    		String mailTo=props.getProperty("mailTo");
    		String mailCc=props.getProperty("mailCC");
			System.out.println(" host "+host +" port "+ port +" mailFrom  "+mailFrom + " to email "+ mailTo+ " attachmenet path "+ emailAttachmentPath +" password "+password);
			// message info
	        String subject = "E-Invoice from Mann+Hummel";
	        String message = "Please find the processed invoice";
	 
	        // attachments
	        String[] attachFiles = new String[1];
	        attachFiles[0] = emailAttachmentPath;
	        sendEmailWithAttachments(host, port, mailFrom, mailTo,mailCc,subject, message, attachFiles,password);
	        log.info("EmailSender -> Email Sent");
	       
    	}
    	catch (IOException e1) {
			log.info(" EmailSender -> sendEmail() -> IOException ->  "+e1.toString());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	catch (Exception e1) {
			log.info(" EmailSender -> sendEmail() -> Exception ->  "+e1.toString());
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    }
 
    /**
     * Test code for to sending e-mail with attachments
     */
    public static void main(String[] args) {
    	
        // SMTP info  								#Mann-Hummel info
        String host = "smtp.gmail.com";				//String host="mhrelay.corp.mann-hummel.com";
        String port = "587";
        //String mailFrom="taegutec2017@gmail.com";
        //String password="taegutec@2017";// String port="25";
       String mailFrom = "idlemonitorapp@gmail.com"; //String mailFrom="RASHMI.MK@MANN-HUMMEL.COM";
       
        String password = "iDlem0nit0r";           // when we publishing there server, password is not required
 
        // message info
        String mailTo = "bhagya@knstek.com";
        String mailCc="raavibhagya@gmail.com";
        String subject = "E-Invoice from Mann+Hummel";
        String message = "I have some attachments for you.";
 
        // attachments
        String[] attachFiles = new String[1];
        attachFiles[0] = "D:\\KNS Work\\Projects\\Digital Signature\\Mann-Hummel\\Sample Invoices\\89135760.pdf";
        
 
        try {
            sendEmailWithAttachments(host, port, mailFrom, mailTo,mailCc,
                subject, message, attachFiles,password);
            System.out.println("Email sent.");
        } catch (Exception ex) {
            System.out.println("Could not send email.");
            ex.printStackTrace();
        }
    }
}